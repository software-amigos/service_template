# Purpose

This document explains the general structure of a microservice.

# Overview

Features within the system are subdivided as much as reasonably possible into different microservices. While the responsibilities of each should be unique, their structure should be similar so developers can seamlessly transition between them. The primary goal of this document is to establish a structure for all microservices.

Microservices are currently run as NodeJS + ExpressJS applications written in TypeScript.

# Core Concepts

The microservice design is centered around the Clean Architecture concept, where the application is separated into layers. The outer layers may depend on the interior layers, but not vice versa. In this case, dependency refers to an instance of an object directly requiring / importing another object (as opposed to depending on an interface and having that object passed to it via dependency injection). The idea is that inner layers change less frequently, but when they do change those changes could ripple to the outer layers. On the other hand, outer layers change more frequently but those changes should not ripple inwards.

![Clean Architecture](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)

Note that we still allow objects in interior layers to depend on interfaces of objects in other layers. Why? Consider the ‘entities’ layer. Entities (or models) are objects which are core data items for the enterprise/system/application. Loosely speaking, it represents an item that would be stored in a database. Entities are responsible for ensuring the data it holds is valid for that object (i.e. fits the business rules).

A user entity, for instance, commonly has a field for an email address. All user entities are expected to have a valid email so it is the user entity’s job to validate that data. In a world full of (mostly) robust libraries, some of which certainly include email validation, should you feel the need to write your own email address validation code within that user entity? Absolutely not - that's a waste of time. Should you import that library directly into your user entity? Absolutely not - that is a strong coupling between your inner most layer and an outer layer. Instead, your entity should depend on an interface for what an email validation object or function looks like so that an object which implements that interface can be passed in. If your email validation method needs to change, you only need to change the validation object being passed into the entity instead of modifying the entity itself (i.e. software which is open to extension but closed to modification).

# Folder Structure

The folder structure is intended to implement the Clean Architecture format but doesn’t use the same wording and the layering may not be inherently clear; this section should help align the terminology. Note that not every folder you see in the repo may be documented here; doing so would be unnecessarily cumbersome and prone to being outdated often. We’ll just talk about some of the key stuff here.

## Project Root [ / ]

Plain files stored here are generally configuration files for the packages used in the microservice (e.g. linting, code formatting, compilation settings, etc).

## Source [ /src/ ]

The source folder which contains the meat of the production application. The below diagram depicts how each folder falls into Clean Architecture:

![Folder Strucutre](./assets/design-overview/folder-structure.png)

Notice that there is one folder floating beneath even the business rules layer - errors. Errors are a part of the language itself and though there are a few different types of predefined errors, they are often not enough to accurately describe an issue that occurred in your application. Throwing and catching the right errors at the right time can drastically simplify diagnosing and managing code issues, user input issues, etc. Because of this, any and every layer may directly depend on custom errors.

### Application Entry Point [ /src/app.ts ]

This is the main file for our application. It kicks off any initialization that needs to be done for classes, starts the server, sets up middleware for Express, and sets up the routes.

### Utilities [ /src/utils/ ]

Generally classes and functions in here act as wrappers around imported libraries. General helper functions are also a good fit for this folder.

### Databases [ /src/databases/ ]

Classes which are used to directly interact with databases. Databases here are specifically part of our system; a class which interacts with an external API or external database does not belong here.

### Config [ /src/config/ ]

Configuration data for the application. Environment variables, system secrets, etc. should all be accessed from classes in this directory.

### Controllers [ /src/controllers/ ]

Classes or functions which take data from some kind of input (usually an HTTP request) and turn it into a format which can be used by service(s). Generally, there should be one controller per API endpoint. A single controller may rely on multiple services.

Consider an API which is intended to allow an admin to send a notification to all users; there would be a single controller to handle that request but may use services such as GetAllUsers and NotifyUser to complete its task.

### Services [ /src/services/ ]

Modular functions to execute some form of application logic. A service should ideally perform a single action, not multiple.

In the example discussed for controllers, one may question why we don't just have a single service to GetAndNotifyAllUsers. The argument against it is that you you likely already have individual services for those actions, so creating a service which just combines them is unnecessary and can create clutter.

### Models [ /src/models/ ]

Core objects which the system / microservice revolve around. Models are objects that represent the data that we care about; actions of our system tend to revolve around creating models, modifying models, deleting models, associating models, etc. Models are the most base-level rules for our data.

### Errors [ /src/errors/ ]

Custom error types which are used to communicate issues in the system.

## Test [ /test/ ]

This folder stores objects to assist with unit testing (stub classes, mock classes, helper functions, etc). It does not contain any actual tests; unit test files should be stored alongside the source file they are testing. The test folder let’s us show off the beauty of dependency injection and allows us to create fake versions of classes and functions that satisfy an interface, but perhaps give us additional functionality. Details on how unit testing should be performed are not a part of this document, just know that this folder should be used to store functionality that is only required for unit testing purposes.

# Logic Flow

The below diagram shows a very high level flow of logic for a simple request to the microservice:

![Logic Flow](./assets/design-overview/logic-flow.png)

Note that this represents a fairly simple request. True dependencies are well represented here but injected ones are not; for instance, a service within the application logic layer may have a database class injected into it so that it can store data from a model after the model is constructed (since construction of the model indicates that the data is valid). An error being thrown would also alter the logic flow.
