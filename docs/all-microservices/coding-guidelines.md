# Purpose

To enforce some level of consistency in design across features and microservices.

# Overview

The guidelines listed in this document should be followed unless the developer has good justification to deviate from them. These guidelines are intended to keep code looking relatively uniform across all microservices even when written by different developers. Encouraging a common understanding of the design enables quicker feature implementation and easier maintenance.

This guide should be kept as lightweight as possible to encourage developers to abide by it. Whenever possible, rules which can be enforced by an automated tool (such as Prettier or ESLint) should use that tool and do not need to be discussed here; refer to those tool configuration files as needed. This document will focus more on concepts that should be maintained rather than minor code convention preferences.

# Folder Structure

-   **Every folder with source code should have an index file which exports public data/classes/functions/etc.** The index serves as the interface for that folder; anything exported by the index file can be used by other code. Anything that is not exported by the index should be considered private to file and no external code should directly access it.

-   **Every folder with source code should have a types definition file.** An important reason to enforce this rather than including the type definitions in the index file is to prevent circular dependencies. For example, consider a `logger/` folder with `index.ts` and `logger.ts`; the index file would import the logger file so that it can then export some public functionality of the logger. If the `ILogger` interface were defined in the index file, `logger.ts` would also need to import the index file to get access to the interface definition, leading to two files which import each other. Separating type definitions into a new file removes this issue.

# General

-   **Prefer to use `const` instead of `let` and almost never use `var`.** This is enforced by our linter but it's worth noting why. `const` enforces that once you assign a value to the variable, it will not be reassigned (see note). This helps code readability and maintainability because if you know that the initial data assigned to a const value is valid, it will stay valid. `let` allows you to reassign a variable which is useful for something like a counter for a loop. `var` is similar to `let` but its scoping is at the function level instead of the block level (e.g. if you create a `var` variable inside of an if statement in a function, that variable is still accessible to everything else in the function even if it's not in the if statement).

    _**Note**: You can still modify an object stored in a `const` variable, you just can't reassign it (i.e. the reference is constant). If you have a const array you can still push new values into it but you can't assign a new value to that variable._

-   **Prefer to throw and catch errors instead of using return codes.** If you call a function, you can choose to ignore a value returned from it. Consider a function that writes to a database:

    -   `write_return` returns `true` if the write was successful and `false` if it failed
    -   `write_throw` has no return value but throws an error if it failed

    If I use `write_return` and forget to check the return value then the database write could fail and neither the application nor the user knows that something went wrong. Program flow continues until someone tries to access that data and they find it doesn't exist (and then maybe it was just a read error - you're not immediately sure).

    If I use `write_throw` and forget to wrap it in a try/catch block then either a separate try/catch at a higher level is going to catch the error and indicate a problem occurred **or** the application will crash because the error was never caught (although you should **always** have a top level try/catch to avoid this). Either way, there is an early indication that something failed and a fix can be implemented quickly.

-   **Use popular dependencies that are regularly updated.** Having a clean architecture makes it significantly easier to swap out dependencies but you should still avoid depending on obscure libraries that nobody uses. As an added bonus, try to find libraries that have few dependencies on their own.

# Modules

-   **Prefer to create modules centered on functions instead of classes.** Classes are great for grouping together functions that belong together, but not everything needs to be a class. If functionality needs to be tied together because of implementation dependencies, then bundle them in a class (like a password hasher - creating a hashed password will be coupled with how to validate a password because both functions are dependent on the hashing algorithm). If functionality can be separated, consider just exporting single functions instead of classes. The string validators are a good example of things that do not really need to be classes because the interface is just a single function.

# Functions

-   **Write functions of 25 lines or fewer.** Take this with a grain of salt; Prettier is setup to limit lines of code to 80 characters so it may turn one line of code into three or four. The point of this rule is to encourage a developer to at least question the length of the function if it exceeds 25 lines. Going over a couple lines is totally acceptable. Doubling that limit is probably a sign that your function should actually be two or three functions.

-   **Write functions that take 4 or fewer arguments.** Similar to the code lines guideline, this is just to suggest that having too many parameters to a function is generally a sign that the function is doing too much. There are plenty of exceptions, e.g. a database function for inserting data may take 6+ arguments depending on the model. Use your judgement while coding and be mindful of this in code reviews.

# Classes

-   **Write classes with as few functions as reasonably possible.** Classes are great for grouping data or functionality that belongs together, but always challenge yourself by asking "should this go into a separate class / should this just be a standalone function?" Remember the single responsibility principle (SRP): a class should do one thing and do it well.

-   **Write getter functions for class properties; avoid exposing the property itself and avoid setter functions.** A getter is a function that simply returns the value of a property. Using a getter as opposed to a readonly property gives you the flexibility of performing additional steps when getting that data (e.g. maybe you're trying to figure out just how often that data is accessed so you print to the console every time the getter is called). Setter functions are good if you truly need to be able to change the value of a class property (because it lets you validate the data being passed in) but setters still allow you to change the state of an object after it has been created which we would like to avoid. If you need to change a property value, prefer to construct a new instance of the object with the correct value.

-   **When exporting classes, enforce immutability.** An object should never be in an invalid state. It can be an unbelievably cumbersome burden to not know if data in an object is valid, forcing you to perform similar validations all over your code. An object should either be created with valid data or it should not be constructed at all. Once the object is constructed, you should not be able to change the data within the object. A good way of accomplishing this is to write classes that only have getter functions for properties.

# Unit Testing

-   **Write tests that provide value; don't aim for 100% test coverage just to be a perfectionist.** Thinking about every single corner case for every single function or class is a waste of time. Write tests for the most critical or most common cases and move on. At the time of writing this, there are no unit tests for the logger. Why? For one, it's a bit of a pain to test with code that the logger is writing to the console. More importantly, I wouldn't get much value out of those tests. If for some reason our logger stops working then I'm going to notice it while I'm developing. If we notice some weird bug with our logger at some point then we can write a unit test that exercises that bug then fix the bug to ensure we don't have that issue again.

# Patterns to Avoid

-   **Avoid the singleton pattern.** The singleton pattern means you have an object that can only be instantiated one time; attempting to make a new copy of it results in just receiving a reference to the existing copy. This pattern implies some level of global state within your application and should be avoided as much as possible. One place where the singleton is currently implemented, however, is for the Logger module. It's useful to have a singleton here because when we configure the logger to output to a file or output to the console, we generally want it to have that behavior throughout our application. There is little state in this object that can be configured such that changing the state would drastically alter the performance of the application.

-   **Avoid global variables.** Global variables are worse than the singleton pattern. I won't provide a ton of detail here because this is one of the most commonly taught "no-no's" in software; there is also a ton of information online about the unexpected side effects of globals.
