import { IUuidGenerator } from '../../../src/utils/uuid';

export class UuidGeneratorMock implements IUuidGenerator {
  generateReturn: string;
  generateCallCount: number;

  constructor(uuid: string) {
    this.generateReturn = uuid;
    this.generateCallCount = 0;
  }

  generate(): string {
    this.generateCallCount++;
    return this.generateReturn;
  }
}
