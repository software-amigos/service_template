import { IStringValidator } from '../../../src/utils/string-validators';

export class StringValidatorMock implements IStringValidator {
  validateReturn: boolean;
  validateCallCount: number;

  constructor(isValid: boolean) {
    this.validateCallCount = 0;
    this.validateReturn = isValid;
  }

  validate(_input: string): boolean {
    this.validateCallCount++;
    return this.validateReturn;
  }
}
