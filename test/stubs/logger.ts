import { ILogger, LogLevel } from '../../src/utils/logger';

export class LoggerStub implements ILogger {
  fatal(_message: string): void {
    // Do nothing
  }
  error(_message: string): void {
    // Do nothing
  }
  warn(_message: string): void {
    // Do nothing
  }
  info(_message: string): void {
    // Do nothing
  }
  debug(_message: string): void {
    // Do nothing
  }
  trace(_message: string): void {
    // Do nothing
  }
  setLevel(_level: LogLevel): void {
    // Do nothing
  }
  outputToFile(_filePath: string): void {
    // Do nothing
  }
}
