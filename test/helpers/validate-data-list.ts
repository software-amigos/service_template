/**
 * Executes the test function with each item in the test data.
 * If the result ofthe data does not match the expected result,
 * error is thrown identifying the data which caused it to fail.
 *
 * @param testFunction function under test
 * @param testData list of data to test the function against
 * @param expectedResult expected return value of test function for all data
 */
export function validateDataList(
  testFunction: (data: any) => boolean,
  testData: any[],
  expectedResult: boolean,
): void {
  testData.forEach((data) => {
    const result = testFunction(data);
    if (result !== expectedResult)
      throw new Error(
        `Expected ${expectedResult} but received ${result} for item: '${data}'`,
      );
  });
}
