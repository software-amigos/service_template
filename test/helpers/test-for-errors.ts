/**
 * Executes the wrapped function. If execution does not result in an exception,
 * throws an error and lists the data which cause the function not to throw
 *
 * @param wrappedFunction any function that is wrapped in a no-param function
 * @param dataUnderTest data that is being tested within the wrapped function
 */
export function throwOnNoError(
  wrappedFunction: () => void,
  dataUnderTest: any,
) {
  try {
    wrappedFunction();
  } catch (e) {
    // Return without throwing an error
    return;
  }
  throw new Error(
    `Expected error but received none for item: '${dataUnderTest}'`,
  );
}

/**
 * Executes the wrapped function. If execution results in an exception,
 * throws an error and lists the data which cause the function to throw
 *
 * @param wrappedFunction any function that is wrapped in a no-param function
 * @param dataUnderTest data that is being tested within the wrapped function
 */
export function throwOnError(wrappedFunction: () => void, dataUnderTest: any) {
  try {
    wrappedFunction();
  } catch (e) {
    throw new Error(
      `Did not expect error but caught one for item: '${dataUnderTest}' \r\n Received error message: ${e.message}`,
    );
  }
}
