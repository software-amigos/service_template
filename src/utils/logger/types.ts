/*
  FATAL 	Designates very severe error events that will presumably lead the application to abort.
  ERROR 	Designates error events that might still allow the application to continue running.
  WARN 	  Designates potentially harmful situations
  INFO 	  Designates informational messages that highlight the progress of the application at coarse-graie~ned level.
  DEBUG 	Designates fine-grained informational events that are most useful to debug an application.
  TRACE 	Designates finer-grained informational events than the DEBUG
*/

export enum LogLevel {
  FATAL = 'fatal',
  ERROR = 'error',
  WARN = 'warn',
  INFO = 'info',
  DEBUG = 'debug',
  TRACE = 'trace',
}

export interface ILogger {
  fatal(message: string): void;
  error(message: string): void;
  warn(message: string): void;
  info(message: string): void;
  debug(message: string): void;
  trace(message: string): void;
  setLevel(level: LogLevel): void;
  outputToFile(filePath: string): void;
}
