import { makeLog4JSLogger } from './logger';
import { ILogger, LogLevel } from './types';

export { ILogger, LogLevel };
export const logger: ILogger = makeLog4JSLogger();
