import { ILogger, LogLevel } from './types';

import log4js from 'log4js';

class Log4JSLogger implements ILogger {
  #logger: log4js.Logger;

  constructor() {
    log4js.configure({
      appenders: {
        out: { type: 'stdout' },
      },
      categories: { default: { appenders: ['out'], level: 'trace' } },
    });
    this.#logger = log4js.getLogger();
  }

  fatal(message: string): void {
    this.#logger.fatal(message);
  }

  error(message: string): void {
    this.#logger.error(message);
  }

  warn(message: string): void {
    this.#logger.warn(message);
  }

  info(message: string): void {
    this.#logger.info(message);
  }

  debug(message: string): void {
    this.#logger.debug(message);
  }

  trace(message: string): void {
    this.#logger.trace(message);
  }

  setLevel(level: LogLevel): void {
    this.#logger.level = level;
  }

  outputToFile(filePath: string): void {
    log4js.configure({
      appenders: {
        logFile: { type: 'dateFile', filename: filePath },
      },
      categories: { default: { appenders: ['logFile'], level: 'trace' } },
    });
  }
}

export function makeLog4JSLogger(): Log4JSLogger {
  return new Log4JSLogger();
}
