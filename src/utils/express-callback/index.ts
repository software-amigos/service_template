import { IExpressCallback, IMakeExpressCallback } from './types';
import { buildMakeExpressCallback } from './express-callback';
import { logger } from '../logger';

export { IExpressCallback, IMakeExpressCallback };
export const makeExpressCallback: IMakeExpressCallback = buildMakeExpressCallback(
  logger,
);
