import { IMakeExpressCallback } from './types';
import { IControllerFunction } from '../../controllers';
import { Request, Response } from 'express';
import {
  IControllerResponse,
  IControllerRequest,
} from '../../controllers/types';
import { DataValidationError } from '../../errors';
import { ILogger } from '../logger';

export function buildMakeExpressCallback(
  logger: ILogger,
): IMakeExpressCallback {
  return function makeExpressCallback(controllerFunction: IControllerFunction) {
    return async function expressCallback(req: Request, res: Response) {
      const controllerRequest: IControllerRequest = {
        body: req.body,
      };
      controllerFunction(controllerRequest)
        .then((controllerResponse: IControllerResponse) => {
          if (typeof controllerResponse.header !== 'object')
            throw new Error(
              `Internal error: response header is not in JSON format`,
            );
          if (typeof controllerResponse.body !== 'object')
            throw new Error(
              `Internal error: response body is not in JSON format`,
            );

          res.set(controllerResponse.header);
          res.status(controllerResponse.statusCode);
          res.send(controllerResponse.body);
        })
        .catch((e) => {
          logger.debug(e.message);
          let messageToUser = e.message;

          if (e instanceof DataValidationError) {
            res.status(400);
          } else {
            messageToUser = `Internal server error 😢 It's our fault, not yours 💗`;
            logger.trace(e.stack);
            res.status(500);
          }

          res.send(`${messageToUser}`);
        });
    };
  };
}
