import { IControllerFunction } from '../../controllers';
import { Request, Response } from 'express';

export interface IExpressCallback {
  (request: Request, response: Response): Promise<void>;
}

export interface IMakeExpressCallback {
  (action: IControllerFunction): IExpressCallback;
}
