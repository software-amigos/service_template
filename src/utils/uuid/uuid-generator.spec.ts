import { makeUuidGenerator, IUuidGenerator } from '.';

describe('A UUID generator', () => {
  let uuidGen: IUuidGenerator;

  beforeEach(() => {
    uuidGen = makeUuidGenerator();
  });

  it(`should generate valid UUID's`, () => {
    for (let i = 0; i < 20; i++) {
      const uuid = uuidGen.generate();
      const matches = uuid.match(
        /[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/,
      );
      expect(matches?.length).toBe(1);
    }
  });
});
