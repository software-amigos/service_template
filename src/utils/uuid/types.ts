export interface IUuidGenerator {
  generate(): string;
}

export interface IMakeUuidGenerator {
  (): IUuidGenerator;
}
