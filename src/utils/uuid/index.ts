import { IUuidGenerator, IMakeUuidGenerator } from './types';
import { buildMakeUuidGenerator } from './uuid-generator';

export { IUuidGenerator, IMakeUuidGenerator };
export const makeUuidGenerator: IMakeUuidGenerator = buildMakeUuidGenerator();
