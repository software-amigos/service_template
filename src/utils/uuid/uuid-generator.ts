import { IUuidGenerator, IMakeUuidGenerator } from './types';

import { v4 as uuidv4 } from 'uuid';

export function buildMakeUuidGenerator(): IMakeUuidGenerator {
  class UuidGenerator implements IUuidGenerator {
    generate(): string {
      return uuidv4();
    }
  }

  return () => new UuidGenerator();
}
