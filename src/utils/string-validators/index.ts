import { buildMakeEmailValidator } from './email-validator';
import { IStringValidator, IMakeStringValidator } from './types';
import { buildMakeUuidValidator } from './uuid-validator';

export { IStringValidator, IMakeStringValidator };
export const makeEmailValidator: IMakeStringValidator = buildMakeEmailValidator();
export const makeUuidValidator: IMakeStringValidator = buildMakeUuidValidator();
