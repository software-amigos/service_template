import { buildMakeEmailValidator } from './email-validator';
import { IStringValidator } from './types';
import { badEmails, goodEmails } from '../../../test/fake-data/emails';
import { validateDataList } from '../../../test/helpers/validate-data-list';

describe('An email validator', () => {
  let emailValidator: IStringValidator;

  beforeEach(() => {
    const makeEmailValidator = buildMakeEmailValidator();
    emailValidator = makeEmailValidator();
  });

  it('should return true for good email addresses', () => {
    expect(() =>
      validateDataList(emailValidator.validate, goodEmails, true),
    ).not.toThrow();
  });

  it('should return false for bad email addresses', () => {
    expect(() =>
      validateDataList(emailValidator.validate, badEmails, false),
    ).not.toThrow();
  });
});
