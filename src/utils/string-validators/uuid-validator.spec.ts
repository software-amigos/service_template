import { buildMakeUuidValidator } from './uuid-validator';
import { IStringValidator } from './types';
import { goodUuids, badUuids } from '../../../test/fake-data/uuids';
import { validateDataList } from '../../../test/helpers/validate-data-list';

describe('A UUID validator', () => {
  let uuidValidator: IStringValidator;

  beforeEach(() => {
    const makeUuidValidator = buildMakeUuidValidator();
    uuidValidator = makeUuidValidator();
  });

  it(`should return true for valid UUID's`, () => {
    expect(() =>
      validateDataList(uuidValidator.validate, goodUuids, true),
    ).not.toThrow();
  });

  it(`should return false for invalid UUID's`, () => {
    expect(() =>
      validateDataList(uuidValidator.validate, badUuids, false),
    ).not.toThrow();
  });
});
