import { IStringValidator, IMakeStringValidator } from './types';

import validator from 'validator';

export function buildMakeEmailValidator(): IMakeStringValidator {
  class EmailValidator implements IStringValidator {
    validate(email: string): boolean {
      return validator.isEmail(email);
    }
  }

  return () => new EmailValidator();
}
