import { IStringValidator, IMakeStringValidator } from './types';

import validator from 'validator';

export function buildMakeUuidValidator(): IMakeStringValidator {
  class UuidValidator implements IStringValidator {
    validate(uuid: string): boolean {
      return validator.isUUID(uuid);
    }
  }

  return () => new UuidValidator();
}
