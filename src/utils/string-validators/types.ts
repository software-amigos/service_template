export interface IStringValidator {
  validate(input: string): boolean;
}

export interface IMakeStringValidator {
  (): IStringValidator;
}
