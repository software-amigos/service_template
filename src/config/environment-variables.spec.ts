import { buildMakeEnvironmentVariables } from './environment-variables';
import { IEnvironmentVariables } from './types';

let environment: IEnvironmentVariables;

beforeEach(() => {
  const makeEnv = buildMakeEnvironmentVariables();
  environment = makeEnv();
});

describe('Environment variables', () => {
  describe('server port', () => {
    it('should be a number within valid port range', () => {
      const serverPort = environment.serverPort();
      expect(serverPort).toBeGreaterThanOrEqual(0);
      expect(serverPort).toBeLessThanOrEqual(65535);
    });
  });

  describe('API root', () => {
    it(`should not contain whitespace`, () => {
      const apiRoot = environment.apiRoot();
      const hasWhitespace = /\s/.test(apiRoot);
      expect(hasWhitespace).toEqual(false);
    });
  });
});
