export interface IEnvironmentVariables {
  serverPort(): number;
  apiRoot(): string;
}

export interface IMakeEnvironmentVariables {
  (): IEnvironmentVariables;
}
