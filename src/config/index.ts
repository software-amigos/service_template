import { IEnvironmentVariables } from './types';
import { buildMakeEnvironmentVariables } from './environment-variables';

const makeEnvironmentVariables = buildMakeEnvironmentVariables();

export { IEnvironmentVariables };
export const environment: IEnvironmentVariables = makeEnvironmentVariables();
