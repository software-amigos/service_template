import dotenv, { DotenvConfigOutput } from 'dotenv';
import { IEnvironmentVariables, IMakeEnvironmentVariables } from './types';

export function buildMakeEnvironmentVariables(): IMakeEnvironmentVariables {
  class EnvironmentVariables implements IEnvironmentVariables {
    #envFound: DotenvConfigOutput;

    constructor() {
      const path = process.cwd();
      this.#envFound = dotenv.config({ path: path + '/.env' });
      if (this.#envFound.error)
        throw new Error(
          `Error loading environment variables; could not find a .env file in ${path}`,
        );
    }

    serverPort(): number {
      return parseInt(<string>process.env['SERVER_PORT'], 10);
    }

    apiRoot(): string {
      return <string>process.env['API_ROOT'];
    }
  }

  return () => new EnvironmentVariables();
}
