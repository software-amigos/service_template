import { logger } from './utils/logger';
import { environment } from './config/';
import express from 'express';
// import { makeExpressCallback } from './utils/express-callback';

async function startServer() {
  const app = express();
  const port = environment.serverPort();
  // const apiRoot = environment.apiRoot();

  app.use(express.json());
  app.use((req, _res, next) => {
    logger.info(`REQUEST: ${req.method} ${req.url} from ${req.ip}`);
    next();
  });

  // START: Application routes

  // END: Application routes

  app.listen(port, (e) => {
    if (e) throw e;
  });
  logger.info(`Server listening on port ${port}`);
}

try {
  startServer();
} catch (e) {
  logger.fatal(e);
  process.exit();
}

process.on('SIGINT', () => {
  logger.fatal('App aborted...');
  process.exit();
});
