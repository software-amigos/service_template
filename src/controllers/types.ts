/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * A note on the 'any' type for request and response:
 * We are allowing this type here for a few reasons:
 * 1. It is the same type that Express uses for req.body
 * 2. Generally, we expect a JSON object for these values
 * 2.a. For requests, this can be enforced via code if using a JSON body parser middleware for all requests
 * 2.b. For responses, this can be enforced by checking that the typeof response.body is 'object'
 * 3. We are not using the 'object' type because:
 * 3.a. Eslint recommends against using 'object' and considers it an illegal type
 * 3.b. Using the 'object' type causes more eslint errors if you try to access any property of that object (because it assumes that object will never have it)
 */

export interface IControllerRequest {
  readonly body: any;
}

export interface IControllerResponse {
  readonly body: any;
  readonly header: any;
  readonly statusCode: number;
}

export interface IControllerFunction {
  (request: IControllerRequest): Promise<IControllerResponse>;
}
