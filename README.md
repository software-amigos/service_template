# Service Name

## Description

This section should be used to provide a brief description of the service.

---

## Setup

Install the following applications:

-   [Git](http://git-scm.com/)
    -   Git LFS must also be initialized using `git lfs install`
-   [Node.js](http://nodejs.org/) (with NPM)

The service is a NodeJS + ExpressJS application. Its dependencies are installed and managed using NPM.

1. `git clone <repository-url>` the repository
1. change into the new directory
1. `npm install`
1. Create a `.env` file in the root of the project directory; use the `.env.example` as a reference for variables to set

---

## Running

1. In one terminal: `npm run watch-ts` to build JS files from TS files (auto rebuild when TS file changes are detected)
1. In another terminal: `npm run start` to start the server (auto restart when JS file changes are detected)

_Note: when making significant changes such as moving entire directories of source files, it is suggested to stop the watch-ts command, run `npm run clean-ts` to clear the `dist/` folder, then re-run `npm run watch-ts`_

---

## Testing

1. Build using either `npm run build-ts` or `npm run watch-ts`
1. Run unit tests using `npm run test`

---

## Additional Documents

Further documentation, such as microservice design, coding guidelines, and other information, can be found in the `docs/` folder. Git submodules were considered for hosting docs to prevent duplication of files across microservice repos, but currently the size of the docs folder is small and submodules seem too cumbersome for now.

-   `docs/all-microservices/` contains information related to microservices in general
-   `docs/this-microservice/` contains information related to the specific microservice

---

## NPM Commands

The table below lists the commands which can be run in the following format:

`npm run [command]`

| Command    | Description                                                                                                      |
| ---------- | ---------------------------------------------------------------------------------------------------------------- |
| `watch-ts` | Start a process to watch the source code for changes, then performs a typescript build when a change is detected |
| `build-ts` | Perform a single typescript build                                                                                |
| `clean-ts` | Empty the contents of the build output directory                                                                 |
| `start`    | Start the server application, then restarts the server whenever a change is detected on the javascript code      |
| `test`     | Run project unit tests                                                                                           |
| `lint`     | Run the linting tool to check for errors                                                                         |
| `lint-fix` | Run the linting tool to check for (and attempt to automatically fix) error                                       |
